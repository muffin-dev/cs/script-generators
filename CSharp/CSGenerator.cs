﻿namespace MuffinDev.ScriptGenerators.CSharp
{

	///<summary>
	/// Utility class for generating C# code.
	///</summary>
	public class CSGenerator : TGenerator<CSGeneratorBuilder> { }

}