﻿using System;
using System.Collections.Generic;

using MuffinDev.ScriptGenerators.Utilities;

namespace MuffinDev.ScriptGenerators.CSharp
{

    ///<summary>
    /// Generates an enum.
    ///</summary>
    public class CSEnum : CSGeneratorBuilder
    {

        #region Properties

        // Defines the accessibility level of this enumeration
        private EAccessibilityLevel m_AccessibilityLevel = EAccessibilityLevel.Public;
        
        // The typename of the enumeration
        private string m_EnumName = null;

        // The list of all enum values
        public Dictionary<string, int?> m_EnumItems = new Dictionary<string, int?>();

        // Defines if this enum represents flags (if true, the defined value of each item will be overriden)
        public bool m_IsFlagsEnum = false;

        #endregion


        #region Initialization

        /// <summary>
        /// Generates an enumeration with the given name.
        /// </summary>
        public CSEnum(string _EnumName, EAccessibilityLevel _AccessibilityLevel = EAccessibilityLevel.Public)
        {
            m_EnumName = _EnumName;
            m_AccessibilityLevel = _AccessibilityLevel;
        }

        /// <summary>
        /// Generates an enumeration with the given name and the given values.
        /// </summary>
        /// <param name="_EnumName">The typename of the enumeration to generate.</param>
        /// <param name="_Items">The list of all the items of this enumeration.</param>
        /// <param name="_AccessibilityLevel">The accessibility level of the enumeration.</param>
        public CSEnum(string _EnumName, IList<string> _Items, EAccessibilityLevel _AccessibilityLevel = EAccessibilityLevel.Public)
        {
            m_EnumName = _EnumName;
            foreach (string value in _Items)
                AddItem(value);
            m_AccessibilityLevel = _AccessibilityLevel;
        }

        /// <summary>
        /// Generates an enumeration with the given name and the given values.
        /// </summary>
        /// <param name="_EnumName">The typename of the enumeration to generate.</param>
        /// <param name="_Items">The list of all the items of this enumeration.</param>
        /// <param name="_IsFlagEnum">If true, this enum represents flags. In that case, a Flag attribute will be added, and the values of
        /// each item will be overriden at generation step.</param>
        /// <param name="_AccessibilityLevel">The accessibility level of the enumeration.</param>
        public CSEnum(string _EnumName, IList<string> _Items, bool _IsFlagEnum, EAccessibilityLevel _AccessibilityLevel = EAccessibilityLevel.Public)
        {
            m_EnumName = _EnumName;
            m_IsFlagsEnum = _IsFlagEnum;
            foreach (string value in _Items)
                AddItem(value);
            m_AccessibilityLevel = _AccessibilityLevel;
        }

        #endregion


        #region Public API

        /// <summary>
        /// Adds the named value to the enumeration.
        /// </summary>
        /// <param name="_ItemName">The name of the value to add. It must follow the naming rules for C# enumerations, which means that it
        /// must only contain alphanumeric characters, underscores and it must not start with a number.</param>
        /// <returns>Returns true if the item has been added successfully, or false if another item with the same name is already in the
        /// list.</returns>
        public bool AddItem(string _ItemName)
        {
            m_EnumItems.Add(_ItemName, null);
            return true;
        }

        /// <summary>
        /// Adds the named value to the enumeration, and enforces its value.
        /// </summary>
        /// <param name="_ItemName">The name of the value to add. It must follow the naming rules for C# enumerations, which means that it
        /// must only contain alphanumeric characters, underscores and it must not start with a number.</param>
        /// <param name="_Value">The value you want to set for this item. Note that this value will be overriden if you enable the
        /// IsFlagsEnum option, and could be overriden if another item with the same value is already in the list.</param>
        /// <returns>Returns true if the item has been added successfully, or false if another item with the same name is already in the
        /// list.</returns>
        public bool AddItem(string _ItemName, int _Value)
        {
            if (!m_EnumItems.ContainsValue(_Value))
            {
                m_EnumItems.Add(_ItemName, _Value);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Generates the content using the current settings.
        /// </summary>
        /// <returns>Returns the generated content.</returns>
        public override void Generate(Content _Content)
        {
            if (string.IsNullOrEmpty(m_EnumName))
                return;

            // Add [Flags] attribute if required
            if (m_IsFlagsEnum)
                _Content += $"[{CSUtility.GetAttributeName<FlagsAttribute>()}]";

            _Content += $"{CSUtility.ToString(m_AccessibilityLevel)} enum {m_EnumName}";
            _Content += "{";
            _Content.IndentLevel++;

            // Generate the items depending on the settings
            if (m_IsFlagsEnum)
                GenerateFlags(_Content);
            else
                GenerateItems(_Content);

            _Content.IndentLevel--;
            _Content += "}";
        }

        /// <summary>
        /// Gets/sets the typename of this enumeration.
        /// </summary>
        public string EnumName
        {
            get { return m_EnumName; }
            set { m_EnumName = value; }
        }

        /// <summary>
        /// Gets the number of items to generate for this enumeration.
        /// </summary>
        public int ItemsCount
        {
            get { return m_EnumItems.Count; }
        }

        /// <summary>
        /// Defines if this enumeration represents flags.
        /// </summary>
        public bool IsFlagsEnum
        {
            get { return m_IsFlagsEnum; }
            set { m_IsFlagsEnum = value; }
        }

        #endregion


        #region Internal API

        /// <summary>
        /// Generates the items of the enum as flags.
        /// </summary>
        private void GenerateFlags(Content _Content)
        {
            int pow = 0;
            foreach (KeyValuePair<string, int?> item in m_EnumItems)
            {
                if (!string.IsNullOrEmpty(item.Key))
                {
                    _Content += $"{item.Key} = {Math.Pow(2, pow)},";
                    pow++;
                }
            }
        }

        /// <summary>
        /// Generates the items of the enum.
        /// </summary>
        private void GenerateItems(Content _Content)
        {
            foreach (KeyValuePair<string, int?> item in m_EnumItems)
            {
                if (!string.IsNullOrEmpty(item.Key))
                {
                    string line = item.Key;
                    if (item.Value != null)
                        line += $" = {item.Value}";
                    _Content += $"{line},";
                }
            }
        }

        #endregion

    }

}