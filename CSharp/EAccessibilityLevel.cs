﻿namespace MuffinDev.ScriptGenerators.CSharp
{

    /// <summary>
    /// Defines the accessibility level of a type or a type member.
    /// Values documentation is copied from MSDN:
    /// https://docs.microsoft.com/fr-fr/dotnet/csharp/programming-guide/classes-and-structs/access-modifiers
    /// </summary>
    public enum EAccessibilityLevel
    {
        // The type or member can be accessed by any other code in the same assembly or another assembly that references it
        Public,

        // The type or member can be accessed only by code in the same class or struct.
        Private,

        // The type or member can be accessed only by code in the same class, or in a class that is derived from that class.
        Protected,

        // The type or member can be accessed by any code in the same assembly, but not from another assembly.
        Internal,

        // The type or member can be accessed by any code in the assembly in which it's declared, or from within a derived class in
        // another assembly.
        ProtectedInternal,

        // The type or member can be accessed only within its declaring assembly, by code in the same class or in a type that is derived
        // from that class. Note that this modifier is only available for C# 7.2 and later.
        PrivateProtected
    }

}
