﻿using System;

namespace MuffinDev.ScriptGenerators.CSharp
{

    public static class CSUtility
    {

        public const string ATTRIBUTE_SUFFIX = "Attribute";

        /// <summary>
        /// Gets the usage name of an attribute (by removing the "Attribute" suffix if it exists).
        /// </summary>
        /// <typeparam name="T">The type of the attribute you want to get the usage name.</typeparam>
        /// <returns>Returns the processed type name.</returns>
        public static string GetAttributeName<T>()
            where T : Attribute
        {
            return GetAttributeName(typeof(T));
        }

        /// <summary>
        /// Gets the usage name of an attribute (by removing the "Attribute" suffix if it exists).
        /// </summary>
        /// <param name="_AttributeType">The type of the attribute you want to get the usage name.</param>
        /// <returns>Returns the processed type name.</returns>
        public static string GetAttributeName(Type _AttributeType)
        {
            string typeName = _AttributeType.Name;
            int attrSuffixIndex = typeName.LastIndexOf(ATTRIBUTE_SUFFIX);

            return attrSuffixIndex > 0
                ? typeName.Substring(0, attrSuffixIndex)
                : typeName;
        }

        /// <summary>
        /// Converts the given EAccessibilityLevel value into a string, as it should be written in a C# script.
        /// </summary>
        /// <param name="_AccessibilityLevel">The value you want to convert.</param>
        public static string ToString(EAccessibilityLevel _AccessibilityLevel)
        {
            string output;
            switch (_AccessibilityLevel)
            {
                case EAccessibilityLevel.Protected:
                output = "protected";
                break;

                case EAccessibilityLevel.Private:
                output = "private";
                break;

                case EAccessibilityLevel.ProtectedInternal:
                output = "protected internal";
                break;

                case EAccessibilityLevel.PrivateProtected:
                output = "private protected";
                break;

                case EAccessibilityLevel.Internal:
                output = "internal";
                break;

                default:
                output = "public";
                break;
            }
            return output;
        }

    }

}
