﻿using System;
using System.Collections.Generic;

namespace MuffinDev.ScriptGenerators
{

    ///<summary>
    /// This utility is meant to generate several files, based on its generators list.
    ///</summary>
    public class BatchGenerator
    {

        #region Subclasses

        /// <summary>
        /// Contains informations about the generation of several files.
        /// </summary>
        public struct BatchGeneratorResult
        {
            // Contains the result of all generation operations
            public Generator.GeneratorOperationsList[] operationLists;

            /// <summary>
            /// Filters the operations in the list to get only the successful or errored ones.
            /// </summary>
            /// <param name="_OnlySuccessful">If true, returns only the successful operations, otherwise returns only the errored
            /// ones.</param>
            /// <param name="_IncludeBuilderOperations">If true, includes the operations relative to used builders, otherwise ignore them
            /// in the resulting list.</param>
            /// <returns>Returns the list of filtered operations.</returns>
            public Generator.GeneratorOperation[] FilterOperations(bool _OnlySuccessful, bool _IncludeBuilderOperations = false)
            {
                List<Generator.GeneratorOperation> filteredOperations = new List<Generator.GeneratorOperation>();
                foreach (Generator.GeneratorOperationsList list in operationLists)
                {
                    filteredOperations.AddRange(list.FilterOperations(_OnlySuccessful, _IncludeBuilderOperations));
                }
                return filteredOperations.ToArray();
            }

            /// <summary>
            /// Converts this BatchGeneratorResult instance into a boolean.
            /// </summary>
            /// <param name="_Result">Returns true if the operation is successful, otherwise false.</param>
            public static implicit operator bool (BatchGeneratorResult _Result)
            {
                return _Result.Success;
            }

            /// <summary>
            /// Checks if all the operations are successful. If at least one of them failed, returns false.
            /// </summary>
            public bool Success
            {
                get
                {
                    foreach (Generator.GeneratorOperationsList list in operationLists)
                    {
                        if (!list)
                            return false;
                    }
                    return true;
                }
            }
        }

        #endregion


        #region Properties

        // The list of all referenced generators, where keys are the file paths, and values are the generator instances
        private Dictionary<string, Generator> m_Generators = new Dictionary<string, Generator>();

        #endregion


        #region Public API

        /// <summary>
        /// Adds a generator instance, meant to generate the file at the given absolute path.
        /// </summary>
        /// <param name="_Generator">The Generator to use to generate the file.</param>
        /// <param name="_AbsoluteFilePath">The absolute path to the file to generate. Note that the path will be created if it doesn't exist,
        /// and the file will be replaced if it already exists.</param>
        /// <returns>Returns the generator instance you've just added to this BatchGenerator.</returns>
        public Generator AddGenerator(Generator _Generator, string _AbsoluteFilePath)
        {
			if (m_Generators.TryGetValue(_AbsoluteFilePath, out Generator value))
            {
				throw new ArgumentException("The file path for which you are trying to add a generator already exists. You can't have more than one Generator per file to generate.");
            }

			m_Generators.Add(_AbsoluteFilePath, _Generator);
			return _Generator;
        }

        /// <summary>
        /// Run all referenced generators.
        /// </summary>
        /// <param name="_StopOnError">If true, stop generating files if an error occurs. Otherwise, the errored operation is just skipped,
        /// and other files are generated.</param>
        /// <returns>Returns complete details about the operation.</returns>
		public BatchGeneratorResult Generate(bool _StopOnError = false)
        {
            List<Generator.GeneratorOperationsList> operationsLists = new List<Generator.GeneratorOperationsList>();
            foreach (KeyValuePair<string, Generator> generator in m_Generators)
            {
                Generator.GeneratorOperationsList list = generator.Value.Generate(generator.Key);
                operationsLists.Add(list);

                if (!list && _StopOnError)
                    break;
            }

            return new BatchGeneratorResult { operationLists = operationsLists.ToArray() };
        }

		#endregion

	}

}