﻿using System;

namespace MuffinDev.ScriptGenerators.Utilities
{

    public static class StringExtension
    {

        /// <summary>
        /// Creates a string that contains x times the input string.
        /// </summary>
        /// <param name="_String">The string you want to repeat.</param>
        /// <param name="_Iterations">The number of times you want to repeat the given string.</param>
        /// <returns>Returns the processed string.</returns>
        public static string Repeat(this string _String, int _Iterations)
        {
            string output = string.Empty;
            if (string.IsNullOrEmpty(_String))
                return output;

            _Iterations = Math.Max(_Iterations, 0);
            for (int i = 0; i < _Iterations; i++)
                output += _String;
            return output;
        }

    }

}
