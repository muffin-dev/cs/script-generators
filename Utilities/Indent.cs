﻿using System;

namespace MuffinDev.ScriptGenerators.Utilities
{

    public class Indent
    {

        #region Properties

        public const int MIN_INDENT_LEVEL = 0;
        public const int MIN_INDENT_CHARS = 1;
        public const int DEFAULT_NB_SPACES = 4;
        public const int DEFAULT_NB_TABULATIONS = 1;

        private int m_IndentLevel = MIN_INDENT_LEVEL;
        private bool m_UseSpaces = false;
        private int m_NbCharsPerIndentLevel = DEFAULT_NB_TABULATIONS;

        #endregion


        #region Initialization

        /// <summary>
        /// Creates an instance of Indent, with an indent level of 0. Uses tabulations for indentation.
        /// </summary>
        public Indent()
        {
            IndentLevel = MIN_INDENT_LEVEL;
        }

        /// <summary>
        /// Creates an instance of Indent, with the given indent level. Uses tabulations for indentation.
        /// </summary>
        public Indent(int _IndentLevel)
        {
            IndentLevel = _IndentLevel;
        }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        public Indent(Indent _Other)
        {
            m_IndentLevel = _Other.IndentLevel;
            m_UseSpaces = _Other.m_UseSpaces;
            m_NbCharsPerIndentLevel = _Other.m_NbCharsPerIndentLevel;
        }

        #endregion


        #region Public API

        /// <summary>
        /// Make this Indent instance use spaces to write the indentation, by adding the given number of spaces per each indent level.
        /// </summary>
        public void UseSpaces(int _SpacesPerIndentLevel = DEFAULT_NB_SPACES)
        {
            m_UseSpaces = true;
            m_NbCharsPerIndentLevel = Math.Max(_SpacesPerIndentLevel, MIN_INDENT_CHARS);
        }

        /// <summary>
        /// Make this Indent instance use tabulations to write the indentation.
        /// </summary>
        public void UseTabulations(int _TabulationsPerIndentLevel = DEFAULT_NB_TABULATIONS)
        {
            m_UseSpaces = false;
            m_NbCharsPerIndentLevel = Math.Max(_TabulationsPerIndentLevel, MIN_INDENT_CHARS);
        }

        /// <summary>
        /// Gets/sets the current indent level.
        /// </summary>
        public int IndentLevel
        {
            get { return m_IndentLevel; }
            set { m_IndentLevel = Math.Max(value, MIN_INDENT_LEVEL); }
        }

        #endregion


        #region Operators

        /// <summary>
        /// Converts this Indent instance into a string, which contains tabulations or spaces depending on the settings and the current
        /// indent level.
        /// </summary>
        public override string ToString()
        {
            return (m_UseSpaces ? " " : "\t").Repeat(m_NbCharsPerIndentLevel * m_IndentLevel);
        }

        /// <summary>
        /// Increment the current indent level.
        /// </summary>
        public static Indent operator ++(Indent _Indent)
        {
            _Indent.IndentLevel++;
            return _Indent;
        }

        /// <summary>
        /// Decrement the current indent level.
        /// </summary>
        public static Indent operator --(Indent _Indent)
        {
            _Indent.IndentLevel--;
            return _Indent;
        }

        /// <summary>
        /// Converts this Indent instance into a number, which is equal to the current indent level.
        /// </summary>
        public static implicit operator int(Indent _Indent)
        {
            return _Indent.m_IndentLevel;
        }

        #endregion

    }

}
