﻿using System.Collections.Generic;

namespace MuffinDev.ScriptGenerators.Utilities
{

    /// <summary>
    /// This utility makes easier the redaction of a file.
    /// </summary>
    public class Content
    {

        #region Properties

        // Indentation level and settings
        private Indent m_Indent = null;

        // The list of all lines to write in a file
        private List<string> m_Lines = new List<string>();

        #endregion


        #region Initialization

        /// <summary>
        /// Creates a default Content instance.
        /// Note that the indentation uses tabulations by default.
        /// </summary>
        public Content()
        {
            m_Indent = new Indent();
        }

        /// <summary>
        /// Creates a Content instance, with custom indentation settings.
        /// </summary>
        /// <param name="_Indent"></param>
        public Content(Indent _Indent)
        {
            m_Indent = _Indent != null ? _Indent : new Indent();
        }

        #endregion


        #region Public API

        /// <summary>
        /// Adds a new line to the content.
        /// </summary>
        /// <param name="_Content">The content of the line (without indentation or new line characters).</param>
        public void Line(string _Content)
        {
            m_Lines.Add(m_Indent.ToString() + _Content);
        }

        /// <summary>
        /// Adds the given amount of empty lines to the content.
        /// </summary>
        public void Space(int _NbLines = 1)
        {
            for (int i = 0; i < _NbLines; i++)
                m_Lines.Add(string.Empty);
        }

        /// <summary>
        /// The settings and level of the indentation.
        /// </summary>
        public Indent Indent
        {
            get { return m_Indent; }
        }

        /// <summary>
        /// Gets/sets the current indentation level. To set indentation options (use spaces/tabs, ...), please use the Indent property.
        /// </summary>
        public int IndentLevel
        {
            get { return m_Indent.IndentLevel; }
            set { m_Indent.IndentLevel = value; }
        }

        /// <summary>
        /// Checks if this content is empty or not.
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                foreach (string line in m_Lines)
                {
                    if (!string.IsNullOrEmpty(line))
                        return false;
                }
                return true;
            }
        }

        /// <summary>
        /// Gets the array of all the stored lines.
        /// </summary>
        public string[] Lines
        {
            get { return m_Lines.ToArray(); }
        }

        #endregion


        #region Operators

        /// <summary>
        /// Converts this Content instance into a string, which is the content of a file to generate.
        /// </summary>
        public override string ToString()
        {
            string output = string.Empty;
            foreach (string line in m_Lines)
            {
                if (string.IsNullOrEmpty(output))
                    output = line;

                else
                    output += "\n" + line;
            }

            return output;
        }

        /// <summary>
        /// Adds a new line to the content.
        /// </summary>
        /// <param name="_Content">The content to which you want to add a line.</param>
        /// <param name="_Line">The content of the new line to add.</param>
        /// <returns>Returns the modified Content instance.</returns>
        public static Content operator +(Content _Content, string _Line)
        {
            _Content.Line(_Line);
            return _Content;
        }

        /// <summary>
        /// Converts the Content instance into a string, which is the content of a file to generate.
        /// </summary>
        public static explicit operator string (Content _Content)
        {
            return _Content.ToString();
        }

        #endregion

    }

}
