﻿using MuffinDev.ScriptGenerators.Utilities;

namespace MuffinDev.ScriptGenerators
{

	///<summary>
	/// Base class for creating builders, used by Generators to build a part of a file.
	///</summary>
	public abstract class GeneratorBuilder
	{

		public abstract void Generate(Content _Content);

	}

}