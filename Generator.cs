﻿using System;
using System.IO;
using System.Collections.Generic;

using MuffinDev.ScriptGenerators.Utilities;

namespace MuffinDev.ScriptGenerators
{

	///<summary>
	/// Base class for text or code files generators.
	///</summary>
	public abstract class Generator
	{

		#region Enus & Subclasses

		/// <summary>
		/// Defines the type of a Generator instance operation.
		/// </summary>
		public enum EGeneratorOperationType
		{
			// The operation is meant to generate a directory
			CreateDirectory,

			// The operation is meant to use a builder instance
			UseBuilder,

			// The operation is meant to generate a file
			CreateFile,

			// The operation is meant to replace an existing file
			ReplaceFile
		}

		/// <summary>
		/// Contains informations about an operation performed by a Generator instance.
		/// </summary>
		public struct GeneratorOperation
		{
			// Defines the type of the operation
			public EGeneratorOperationType operationType;

			// The absolute path to the file or directory affected by this operation
			// Null if the operation is a builder usage
			public string path;

			// The eventual error information about a generated file or directory
			public string error;

			/// <summary>
			/// Converts this GeneratorOperation into a boolean, which is true if the operation is successful, otherwise false.
			/// </summary>
			public static implicit operator bool (GeneratorOperation _Operation)
            {
				return _Operation.Success;
            }

			/// <summary>
			/// Checks if this operation succeeded.
			/// </summary>
			public bool Success
            {
                get { return error == null || string.IsNullOrEmpty(error); }
            }
		}

		/// <summary>
		/// Contains informations about all operations performed by a Generator.
		/// </summary>
		public struct GeneratorOperationsList
		{
			// Contains all the operations performed by a Generator
			public GeneratorOperation[] operations;

			/// <summary>
			/// Converts this GeneratorOperationsList into a boolean, which is true if all operations in the list are successful, or false if at
			/// least one of them failed.
			/// </summary>
			public static implicit operator bool(GeneratorOperationsList _OperationsList)
			{
				return _OperationsList.Success;
			}

			/// <summary>
			/// Filters the operations in the list to get only the successful or errored ones.
			/// </summary>
			/// <param name="_OnlySuccessful">If true, returns only the successful operations, otherwise returns only the errored
			/// ones.</param>
			/// <param name="_IncludeBuilderOperations">If true, includes the operations relative to used builders, otherwise ignore them
			/// in the resulting list.</param>
			/// <returns>Returns the list of filtered operations.</returns>
			public GeneratorOperation[] FilterOperations(bool _OnlySuccessful, bool _IncludeBuilderOperations = false)
            {
				List<GeneratorOperation> filteredOperations = new List<GeneratorOperation>();
				foreach (GeneratorOperation operation in operations)
				{
					if ((_OnlySuccessful && !operation.Success) || operation.Success)
						continue;

					if (operation.operationType != EGeneratorOperationType.UseBuilder || _IncludeBuilderOperations)
						filteredOperations.Add(operation);
				}
				return filteredOperations.ToArray();
			}

			/// <summary>
			/// Gets all errors from all operations in the list.
			/// </summary>
			public string[] Errors
            {
				get
                {
					List<string> errors = new List<string>();
					foreach(GeneratorOperation operation in operations)
                    {
						if (!operation)
							errors.Add(operation.error);
                    }
					return errors.ToArray();
                }
            }

			/// <summary>
			/// Returns true if all operations in the list are successful, or false if at least one of them has failed.
			/// </summary>
			public bool Success
            {
                get
                {
					foreach(GeneratorOperation operation in operations)
                    {
						if (!operation)
							return false;
                    }
					return true;
                }
            }
        }

		#endregion


		#region Properties

		// The list of all referenced builders
		private List<GeneratorBuilder> m_Builders = new List<GeneratorBuilder>();

		// The current indentation level
		private Indent m_Indent = null;

		#endregion


		#region Public API

		/// <summary>
		/// Generates the file at the given path using the referenced builers of this Generator.
		/// </summary>
		/// <param name="_AbsoluteFilePath">The absolute path to the file to generate.</param>
		/// <returns>Returns the results of the performed operations.</returns>
		public GeneratorOperationsList Generate(string _AbsoluteFilePath)
        {
			List<GeneratorOperation> operations = new List<GeneratorOperation>();

			// Try to make an accessible directory to contain the file if it doesn't exist yet
			if(!MakeDirectoryAccessible(_AbsoluteFilePath, operations))
				return new GeneratorOperationsList { operations = operations.ToArray() };

			// Generate file content by using all builders
			Content content = new Content();
			try
			{
				if (!GenerateContent(content, m_Builders, operations))
				{
					return new GeneratorOperationsList { operations = operations.ToArray() };
				}
			}
			// If a builder emits an exception, add the errored operation to the list, and cancel the process
			catch (Exception e)
			{
				operations.Add(new GeneratorOperation
				{
					path = _AbsoluteFilePath,
					operationType = EGeneratorOperationType.UseBuilder,
					error = $"{e.GetType().Name}: {e.Message}"
				});
				return new GeneratorOperationsList { operations = operations.ToArray() };
			}

			// Create the file with the processed content
			GeneratorOperation createFileOperation = new GeneratorOperation { operationType = EGeneratorOperationType.CreateFile, path = _AbsoluteFilePath };
			if (!content.IsEmpty)
            {
				try
                {
					File.WriteAllLines(_AbsoluteFilePath, content.Lines);
                }
				// Catch exception when generating the file, and add the errored operation to the list
				catch (Exception e)
                {
					createFileOperation.error = $"{e.GetType().Name}: {e.Message}";
				}
			}
			operations.Add(createFileOperation);

			return new GeneratorOperationsList { operations = operations.ToArray() };
		}

		/// <summary>
		/// Gets/sets the current indentation level.
		/// </summary>
		public int IndentLevel
        {
            get { return m_Indent; }
            set { m_Indent.IndentLevel = value; }
        }

		#endregion


		#region Protected methods

		/// <summary>
		/// Generates file content using the given builders, then writes it in a file at the given path. By default, just adds each builder
		/// output to the file content.
		/// </summary>
		/// <param name="_Content">The output content of the file.</param>
		/// <param name="_Builders">The list of builders to use to generate the content.</param>
		/// <returns>Returns true if the file content has been generated successfully, otherwise false.</returns>
		protected virtual bool GenerateContent(Content _Content, IList<GeneratorBuilder> _Builders, List<GeneratorOperation> _Operations)
		{
			foreach (GeneratorBuilder builder in _Builders)
			{
				GeneratorOperation operation = new GeneratorOperation { operationType = EGeneratorOperationType.UseBuilder };
				try
                {
					builder.Generate(_Content);
                }
				catch (Exception e)
                {
					operation.error = $"{e.GetType().Name}: {e.Message}";
				}

				_Operations.Add(operation);

				if (!operation)
					return false;
			}

			return true;
		}

		/// <summary>
		/// References the given builder for a file to generate.
		/// </summary>
		/// <param name="_Builder">The Builder instance to add to this Generator.</param>
		protected bool AddBuilderInternal(GeneratorBuilder _Builder)
		{
			if (_Builder != null)
			{
				m_Builders.Add(_Builder);
				return true;
			}
			return false;
		}

		/// <summary>
		/// Gets the initial Content container to generate a file. You can inherit from this method to setup custom settings for Content
		/// initialization.
		/// </summary>
		/// <returns>Returns the created Content instance.</returns>
		protected virtual Content MakeContent()
        {
			return new Content();
        }

		#endregion


		#region Private methods

		/// <summary>
		/// Creates the directory at the given path if it doesn't exist, and ensure that directory is writable.
		/// </summary>
		/// <param name="_Path">The path to the directory. If path to a file given, remove the file and extension part.</param>
		/// <param name="_Operations">The list of all operations performed when using this Generator. This list will be filled with the eventual
		/// directory creation operations.</param>
		/// <returns>Returns true if the directory at the given path is accessible, otherwise false.</returns>
		private bool MakeDirectoryAccessible(string _Path, List<GeneratorOperation> _Operations)
		{
			_Path = ToDirectoryPath(_Path);

			if (Directory.Exists(_Path))
				return true;

			string[] pathSplit = _Path.Split(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
			if (pathSplit.Length <= 1)
				return false;

			_Path = _Path.StartsWith(Path.DirectorySeparatorChar.ToString()) || _Path.StartsWith(Path.AltDirectorySeparatorChar.ToString())
				? Path.DirectorySeparatorChar.ToString()
				: string.Empty;

			// For each part of the path
			for (int i = 0; i < pathSplit.Length; i++)
			{
				_Path = Path.Combine(_Path, pathSplit[i]);
				// If the directory doesn't exist, create it
				if (!Directory.Exists(_Path))
				{
					GeneratorOperation operation = new GeneratorOperation { path = _Path, operationType = EGeneratorOperationType.CreateDirectory };
					try
					{
						Directory.CreateDirectory(_Path);
						_Operations.Add(operation);
					}
					catch (Exception e)
					{
						operation.error = $"{e.GetType().Name}: {e.Message}";
						_Operations.Add(operation);
						return false;
					}
				}
			}

			return true;
		}

		/// <summary>
		/// If the given path targets a file, remove its last part to make it target a directory.
		/// </summary>
		/// <param name="_Path">The path you want to convert.</param>
		/// <returns>Returns the processed path.</returns>
		private string ToDirectoryPath(string _Path)
		{
			string[] pathSplit = _Path.Split(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
			// If the given path targets a file, remove the last part of that path
			return (pathSplit[pathSplit.Length - 1].Split('.').Length > 1) ? Path.GetDirectoryName(_Path) : _Path;
		}

		#endregion

	}

}