﻿namespace MuffinDev.ScriptGenerators
{

	///<summary>
	/// Base class for generic text or code files generators.
	///</summary>
	///<typeparam name="TBuilder">The GeneratorBuilder main type supported by this Generator.</typeparam>
	public abstract class TGenerator<TBuilder> : Generator
		where TBuilder : GeneratorBuilder
	{

		/// <summary>
		/// References the given builder for a file to generate.
		/// </summary>
		/// <param name="_Builder">The Builder instance to add to this Generator.</param>
		/// <returns>Returns the builder you've just added.</returns>
		public virtual TBuilder AddBuilder(TBuilder _Builder)
		{
			if (AddBuilderInternal(_Builder))
				return _Builder;
			return null;
		}

	}

}